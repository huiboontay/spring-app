package com.example.userdatabase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    //Create a user
    @RequestMapping("/create")
    public String create(@RequestParam String name, @RequestParam String email, @RequestParam String number){
        User user = userService.create(name, email, number);
        return "Created: " + user.getName();
    }

    @RequestMapping("/get")
    public User getUser(@RequestParam String number){
        return userService.getByNumber(number);
    }

    @RequestMapping("/getAll")
    public List<User> getAll(){
        return userService.getAll();
    }

    @RequestMapping("/update")
    public String update(@RequestParam String name, @RequestParam String email, @RequestParam String number){
        User user = userService.update(name, email, number);
        return user.toString();
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam String number){
        User user = userService.getByNumber(number);
        userService.delete(number);
        return "Deleted: " + user.getName();
    }

    @RequestMapping("/deleteAll")
    public String deleteAll(){
        userService.deleteAll();
        return "Deleted all users!";
    }
}

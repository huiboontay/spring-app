package com.example.userdatabase.data;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Data service for accessing Yahoo
 * @author anna
 */
@Service
@Lazy
public class YahooDataService {

    @Value("${rapidapi.apikey}")
    private String API_KEY;

    @Value("${rapidapi.host.yahoo}")
    private String HOST;

    private List<String> YAHOO_REGION_LIST = new ArrayList<String>(Arrays.asList(
            "US", "AU", "CA", "FR", "DE", "HK", "IT", "ES", "GB", "IN"));

    public List<String> getYahooRegionList() {
        return YAHOO_REGION_LIST;
    }

    public JSONObject getYahooMarketData(String region) {
        String url = String.format("https://%s/market/get-summary?region=%s&lang=en", HOST, region);
        return getData(url);
    }

    public JSONObject getYahooTrendingMarketData(String region) {
        String url = String.format("https://%s/market/get-trending-tickers?region=%s", HOST, region);
        return getData(url);
    }

    public JSONObject getYahooWatchList() {
        String url = String.format("https://%s/market/get-popular-watchlists", HOST);
        return getData(url);
    }

    JSONObject getData(String url) {
        JSONObject res = new JSONObject();
        try {
            HttpResponse<JsonNode> response = Unirest.get(url)
                    .header("x-rapidapi-host", HOST)
                    .header("x-rapidapi-key", API_KEY)
                    .asJson();
            res = response.getBody().getObject();
        } catch (UnirestException ue) {
            System.out.println(ue.getMessage());
        }
        return res;
    }

}

package com.example.userdatabase.data;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The aggregated Data service for RestController quick access
 * @author anna
 */
@Service
public class QueryDataService {

    @Autowired
    YahooDataService yahooDataService;

    @Autowired
    QuandlDataService quandlDataService;

    @Autowired
    BloombergDataService bloombergDataService;

    //Depreciated!
    @Autowired
    CurrencyDataService currencyDataService;

    @Autowired
    AlphaVantageDataService alphaVantageDataService;

    public JSONObject getCurrencyConverterAlpha(String curr1, String curr2) {
        return alphaVantageDataService.getCurrencyConverterFromTo(curr1, curr2);
    }

    public JSONObject getDailyDataFor(String curr1, String curr2) {
        return alphaVantageDataService.getDailyDataFor(curr1, curr2);
    }

    public JSONObject getWeeklyDataFor(String curr1, String curr2) {
        return alphaVantageDataService.getWeeklyDataFor(curr1, curr2);
    }

    public JSONObject getMonthlyDataFor(String curr1, String curr2) {
        return alphaVantageDataService.getMonthlyDataFor(curr1, curr2);
    }

    public JSONObject getCurrencyList() {
        return currencyDataService.getAllCurrencies();
    }

    public JSONObject getCurrencyConverter(String curr1, String curr2, Long amt) {
        return currencyDataService.getCurrencyConverterFromTo(curr1, curr2, amt);
    }

    public JSONObject getHistoricCurrencyConverter(String date) {
        return currencyDataService.getHistoricCurrencyConverterFor(date);
    }

    public List<String> getRegionList() {
        return yahooDataService.getYahooRegionList();
    }

    public JSONObject getMarketDataByRegion(String region) {
        return yahooDataService.getYahooMarketData(region);
    }

    public JSONObject getTrendingMarketByRegion(String region) {
        return yahooDataService.getYahooTrendingMarketData(region);
    }

    public JSONObject getWatchList() {
        return yahooDataService.getYahooWatchList();
    }

    public JSONObject getNewsList() {
        return bloombergDataService.getBloombergNewsData();
    }

    public List<String> getAllTickers() {
        return quandlDataService.getAllQuandlTickers();
    }

    public JSONObject getPriceForTickerFilterByDate(String ticker, String start, String end) {
        return quandlDataService.getQuandlQuery(ticker, start, end);
    }

    public JSONObject getStockMetadata(String ticker) {
        return quandlDataService.getQuandlMetadata(ticker);
    }

}

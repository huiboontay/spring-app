package com.example.userdatabase.data;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Data service for accessing Quandl
 * @author anna
 */
@Service
@Lazy
public class QuandlDataService {

    @Value("${rapidapi.apikey}")
    private String API_KEY;

    @Value("${rapidapi.host.quandl}")
    private String HOST;

    private String QUANDL_TICKERS_FILEPATH = "src/main/resources/static/ticker_list.csv";

    private List<String> AVAILABLE_TICKERS;

    public List<String> getAllQuandlTickers() {
        if (AVAILABLE_TICKERS == null) {
            AVAILABLE_TICKERS = new ArrayList<String>();
            this.buildTickerList();
        }
        return AVAILABLE_TICKERS;
    }

    private void buildTickerList() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(QUANDL_TICKERS_FILEPATH));
            br.readLine();
            String line = br.readLine();
            while (line != null) {
                String[] stock = line.split(",");
                AVAILABLE_TICKERS.add(stock[0]);
                line = br.readLine();
            }
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    public JSONObject getQuandlQuery(String ticker, String startDate, String endDate) {
        String url = String.format("https://%s/datasets/WIKI/%s.json?start_date=%s&end_date=%s",
                HOST, ticker, startDate, endDate);
        return getData(url);
    }

    public JSONObject getQuandlMetadata(String ticker) {
        String url = String.format("https://%s/datasets/WIKI/%s/metadata.json", HOST, ticker);
        return getData(url);
    }

    private JSONObject getData(String url) {
        JSONObject res = new JSONObject();
        try {
            HttpResponse<JsonNode> response = Unirest.get(url)
                    .header("X-RapidAPI-Host", HOST)
                    .header("X-RapidAPI-Key", API_KEY)
                    .asJson();
            res = response.getBody().getObject();
        } catch (UnirestException ue) {
            System.out.println(ue);
        }

        return res;
    }

}

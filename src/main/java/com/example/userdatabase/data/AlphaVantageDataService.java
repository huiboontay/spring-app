package com.example.userdatabase.data;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;


/**
 * Data service for accessing Alpha Vantage
 * @author anna
 */

@Service
@Lazy
public class AlphaVantageDataService {

    @Value("${rapidapi.apikey}")
    private String API_KEY;

    @Value("${rapidapi.host.alphavantage}")
    private String HOST;

    public JSONObject getCurrencyConverterFromTo(String curr1, String curr2) {
        String url = String.format("https://%s/query?function=CURRENCY_EXCHANGE_RATE&to_currency=%s&from_currency=%s",
                HOST, curr2, curr1);
        return getData(url);
    }

    public JSONObject getDailyDataFor(String curr1, String curr2) {
        return getData(getUrl(curr1, curr2, "FX_DAILY"));
    }

    public JSONObject getWeeklyDataFor(String curr1, String curr2) {
        return getData(getUrl(curr1, curr2, "FX_WEEKLY"));
    }

    public JSONObject getMonthlyDataFor(String curr1, String curr2) {
        return getData(getUrl(curr1, curr2, "FX_MONTHLY"));
    }

    public String getUrl(String curr1, String curr2, String function) {
        return String.format("https://%s/query?datatype=json&from_symbol=%s&to_symbol=%s&function=%s",
                HOST, curr1, curr2, function);
    }

    JSONObject getData(String url) {
        JSONObject res = new JSONObject();
        try {
            HttpResponse<JsonNode> response = Unirest.get(url)
                    .header("x-rapidapi-host", HOST)
                    .header("x-rapidapi-key", API_KEY)
                    .asJson();
            res = response.getBody().getObject();
        } catch (UnirestException ue) {
            System.out.println(ue.getMessage());
        }
        return res;
    }

}

# Data Rest API Information

## Queries
To query the Rest Service:
- Use GET requests
- Have API Key in your Request Body
- Prefix all requests with `/data`

### Quandl for stock data
*Note: All queries in this subsection are prefixed with* 
`/data/stock`

**To retrieve tickers which data is available**

`/data/stock/list`

**To query metadata of a stock**

`/data/stock/{ticker}/meta`

- e.g. To get the metadata of Facebook: `/data/stock/FB/meta`
- e.g. To get the metadata of Amazon: `/data/stock/AMZ/meta`

**To query stock prices**

`/data/stock/{ticker}?startDate={start}&endDate={end}`

- e.g. To get the prices of Facebook between 1 June 2014 to 1 July 2014:  
`/data/stock/FB?startDate=2014-05-31&endDate=2014-06-30`
- e.g. To get the prices of Amazon on 1 June 2014: 
`/data/stock/AMZ?startDate=2014-06-01&endDate=2014-05-31`

### Yahoo for market data
*Note: All queries in this subsection are prefixed with* 
`/data/market`

**To retrieve regions which market data is available**

`/data/market/list`

**To gets the summary of market data by regions**

`/data/market/{region}`

- e.g. To get the market data for US: `/data/market/US`

**To gets the trending market data by regions**

`/data/market/{region}/trending`

- e.g. To get the trending market data for US: `/data/market/US/trending`

**To get the popular watchlist**

`/data/market/watchlist`

### Bloomberg for news data
*Note: All queries in this subsection are prefixed with* 
`/data/news`

**To get news data**

`/data/news/list`

### Currency Converter for currency data 
**Depreciated**

*Note: All queries in this subsection are prefixed with* 
`/data/depreciated`

**To get list of currency names**

`/data/depreciated/list`

**To get real time currency rates**

`/data/depreciated/realtime?from={fromCurrency}&to={toCurrency}&amt={amount}`

- e.g. To get currency conversion from SGD to USD for 1 dollar: 
`/data/depreciated/realtime?from=SGD&to=USD&amt=1`

**To get historical currency conversion rates**

`/data/depreciated/historic/{date}`

- e.g. To get currency conversion rate for 2016-01-01: 
`/data/depreciated/historic/2016-01-01`

### Alpha Vantage FX Rates
*Note: All queries in this subsection are prefixed with* 
`/data/currency`

**To get real time conversion rates**
`/data/currency/real?from={fromCurrency}&to={toCurrency}`

**To get Monthly rates for a currency**
`/data/currency/monthly?from={fromCurrency}&to={toCurrency}`

**To get Weekly rates for a currency**
`/data/currency/weekly?from={fromCurrency}&to={toCurrency}`

**To get Daily rates for a currency**
`/data/currency/daily?from={fromCurrency}&to={toCurrency}`
